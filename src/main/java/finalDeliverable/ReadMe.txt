Spell Checker - Group 1 - ReadMe


*** HOW TO USE THE APPLICATION ***

1. Open the application
2. When the application loads, click the “Select File(s) to Check” button and choose the file that you would like to spell check.
3. After you have selected the file to check, click the “Select Dictionary” button and choose the dictionary file that you would like to use to compare your words to.
4. Click the “Start” button to run the program.
5. The misspelled words will appear in the upper left corner in the text box entitled “Misspelled Word”. View the word and decide whether you would like to add it to the dictionary or not. Click on the respective buttons, “Add to Dictionary” or “Do Not Add to Dictionary”.
6. Once the program completes the text box will say “Spell check complete” and the user can then add new files to check.

*** NEED HELP? ***
* “The start button is greyed out, I can’t click on it?”
— The start button will only be enabled once both the files to check and dictionary have been loaded. The program will not run without those files.

* “The add to dictionary and do not add to dictionary buttons are greyed out, I can’t click on it?”
— The add and do not add buttons will only be enabled once you have clicked start. 

