package finalDeliverable;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GUI {

	private JFrame frmSpellChecker;
	private JTextField MispelledWordBox;
	
	private JFileChooser fileChooser = new JFileChooser();
    FileNameExtensionFilter filter = new FileNameExtensionFilter(
            "Text Files", "txt");
	
	static String inputText = "/input/inputText.txt";
	static String addedWords = "/output/addedWords.txt";
	static String dict = "/output/dictionary.txt";
	static String notAddedWords = "/output/notAddedWords.txt";
	
	static ArrayList<String> newlyAddedWords = new ArrayList<String>();
	static ArrayList<String> nonAddedWords = new ArrayList<String>();

	// static ReadFile readFile = new ReadFile();
	
	static int index = 0;
	
	public int fileResult = 0;
	public int dictResult = 0;
	
	public ReadFile fileRead;
	public ReadFile dictRead;
	
	public File selectedDict;
	public File selectedFile;
	
	public Dictionary workingDict;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
	/*	
		ArrayList<String> input = null, dictionary = null, newWords = null;
		
		newWords = new ArrayList<String>();
		
		try {
			
			input = readFile.read(inputText);
			dictionary = readFile.read(dict);
			
		} catch (IOException e) {
			
			
			
		}
		
		for (String word: input) {
			
			newWords.add(word);
			
		}
		
		final ArrayList<String> NEW_WORDS = newWords;
		
		*/
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				try {
					GUI window = new GUI();
					window.frmSpellChecker.setVisible(true);
 				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
				
	}
	
	
	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
				
		// Creates the main frame for the application
		frmSpellChecker = new JFrame();
		frmSpellChecker.setTitle("Spell Checker - V 1.0");
		frmSpellChecker.setBounds(100, 100, 450, 300);
		frmSpellChecker.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Creates the menu bar
		JMenuBar menuBar = new JMenuBar();
		frmSpellChecker.setJMenuBar(menuBar);
		
		// Creates menu bar menu
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		// Creates menu bar menu item - New
		JMenuItem mntmNew = new JMenuItem("New");
		mntmNew.setIcon(new ImageIcon("/Users/StephenBielecki/Desktop/blank-file-xxl.png"));
		mnFile.add(mntmNew);
		
		// Creates menu bar menu item - Save & Quit
		JMenuItem mntmQuit = new JMenuItem("Save & Quit");
		mntmQuit.setIcon(new ImageIcon("/Users/StephenBielecki/Desktop/save.png"));
		mnFile.add(mntmQuit);
		frmSpellChecker.getContentPane().setLayout(new BorderLayout(0, 0));
		
		// Divides main window into two columns
		JPanel MainPanel = new JPanel();
		frmSpellChecker.getContentPane().add(MainPanel, BorderLayout.CENTER);
		MainPanel.setLayout(new GridLayout(1, 2, 0, 0));
		
		// Creates left panel
		JPanel LeftPanel = new JPanel();
		MainPanel.add(LeftPanel);
		LeftPanel.setLayout(new GridLayout(2, 0, 0, 0));
		
		// Creates left upper subpanel
		JPanel LeftUpperPanel = new JPanel();
		FlowLayout fl_LeftUpperPanel = (FlowLayout) LeftUpperPanel.getLayout();
		fl_LeftUpperPanel.setVgap(30);
		LeftPanel.add(LeftUpperPanel);
		
		// Creates mispelled word label
		JLabel MispelledLabel = new JLabel("Mispelled Word");
		LeftUpperPanel.add(MispelledLabel);
		MispelledLabel.setToolTipText("Mispelled Word:");
		
		// Creates text field for word in question
		MispelledWordBox = new JTextField();
		LeftUpperPanel.add(MispelledWordBox);
		MispelledWordBox.setHorizontalAlignment(SwingConstants.CENTER);
		MispelledWordBox.setText("SELECT FILES");
		MispelledWordBox.setColumns(10);
		
		// Creates lower left subpanel
		JPanel LeftLowerPanel = new JPanel();
		LeftLowerPanel.setBackground(UIManager.getColor("Button.background"));
		LeftPanel.add(LeftLowerPanel);
		LeftLowerPanel.setLayout(new GridLayout(3, 1, 0, 0));
		
		// Creates file select button
		final JButton SelectFileButton = new JButton("Select File(s) to Check");
		LeftLowerPanel.add(SelectFileButton);
		SelectFileButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				fileResult = fileChooser.showOpenDialog(SelectFileButton);
				if (fileResult == JFileChooser.APPROVE_OPTION) {
					selectedFile = fileChooser.getSelectedFile();
				}
				
				fileRead = new ReadFile(selectedFile);
			}
		});
	
		
		// Creates dictionary select button
		final JButton SelectDicButton = new JButton("Select Dictionary");
		LeftLowerPanel.add(SelectDicButton);
		SelectDicButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				dictResult = fileChooser.showOpenDialog(SelectDicButton);
				if (dictResult == JFileChooser.APPROVE_OPTION) {
					selectedDict = fileChooser.getSelectedFile();
				}
				
				dictRead = new ReadFile(selectedDict);
			}
		});
		
		// Creates start button
		final JButton btnStart = new JButton("Start");
		btnStart.setForeground(UIManager.getColor("InternalFrame.borderDarkShadow"));
		LeftLowerPanel.add(btnStart);
		btnStart.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				try {
					workingDict = new Dictionary(fileRead.read(), dictRead.read());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				workingDict.getWord();
			}
		});
		
		// Creates right panel
		JPanel RightPanel = new JPanel();
		MainPanel.add(RightPanel);
		RightPanel.setLayout(new GridLayout(4, 1, 0, 0));
		
		// Creates add to dictionary button
		JButton AddButton = new JButton("Add to Dictionary");
		RightPanel.add(AddButton);
		AddButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				workingDict.addToDictionary();
				workingDict.getWord();
			}
		});
		
		// Creates don't add to dictionary button
		JButton NoAddButton = new JButton("Do Not Add to Dictionary");
		RightPanel.add(NoAddButton);
		NoAddButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				workingDict.noAdd();
				workingDict.getWord();
			}
		});
		
		// Creates help button
		JButton HelpButton = new JButton("Help");
		HelpButton.setBackground(UIManager.getColor("Button.background"));
		HelpButton.setForeground(UIManager.getColor("Button.light"));
		RightPanel.add(HelpButton);
		
		// Creates cancel button
		JButton CancelButton = new JButton("Cancel");
		CancelButton.setForeground(UIManager.getColor("Button.select"));
		RightPanel.add(CancelButton);
	}
	
}
