//ReadFile.java  
package finalDeliverable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;  

public class ReadFile {
	
	public File fileName;
	
	ReadFile(File readFile) {
		fileName = readFile;
	}
	
	public ArrayList<String> read() throws IOException {
	    
		BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
		ArrayList<String> words = new ArrayList<String>();
		String line = null;
       
		while ((line = bufferedReader.readLine()) != null) {
			
			for(String word: line.split(" ")) {
				
				String removedPunct = word.replaceAll("[\\W]", "");		// Remove punctuation
				String finalWord = removedPunct.toLowerCase();			// Convert to lowercase
				
				if (!finalWord.trim().equals("")) {
					
					words.add(finalWord);
					
				}
				
			}
			
		}
		
		bufferedReader.close();
		
		return words;
		
	}
	
}  
