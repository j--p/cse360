package finalDeliverable;

import java.util.ArrayList;
import java.lang.String;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Dictionary {
	private ArrayList<String> inputWords;
	private ArrayList<String> dictionaryWords;
	private ArrayList<String> wordsNotAdded;
	private ArrayList<String> wordsAdded;
	private ArrayList<String> wordsToCheck;
	
	private int wordIndex;
	
	// Constructor (list of input words, list of dictionary words)
	Dictionary(ArrayList<String> input, ArrayList<String> dictionary) {
		inputWords = input;
		wordIndex = 0;
		dictionaryWords = alphabetize(0, dictionary.size(), dictionary);
	}
	
	// Binary Search through dictionary
	public boolean binarySearch(ArrayList<String> list, String key) {
		
		boolean found = false;
		int lbound = 0;
		int ubound = list.size();
		int position, comparison;
		
		position = (lbound + ubound) / 2;
		
		while ((list.get(position) != key) && (lbound <= ubound)) {
			comparison = list.get(position).compareTo(key);
			if (comparison > 0) {
				ubound = position - 1;							// Decrease upper bound if word is lexographically earlier
			} else {
				lbound = position + 1;							// Increase lower bound if word is lexographically later
			}
			
			position = (lbound + ubound) / 2;					// Get new position
		}
		
		if (lbound <= ubound) {
			found = true;										// If word is found, set to true
		} else {
			found = false;										// Otherwise, set false
		}
		
		return found;
		
	}
	
	// Compares each word in input list to dictionary list
	public void compare () {
		
		// Iterate through arraylist of input
		for (int i = 0; i < inputWords.size(); i++) {
			boolean found = false;
			boolean repeat = false;
			String checkWord = inputWords.get(i);				// Set checkWord equal to each word in list
			
			found = binarySearch(dictionaryWords, checkWord);	// Run binary search to see if word exists in dictionary
			
			if (found == true) {
				repeat = binarySearch(wordsNotAdded, checkWord);	// Check if word is repeated
				if (repeat == false) {
					wordsNotAdded.add(checkWord);					// Add word to wordsNotAdded if found & not repeated
				}
			} else {
				repeat = binarySearch(wordsToCheck, checkWord);		// Check if word is repeated
				if (repeat == false) {
					wordsToCheck.add(checkWord);					// Otherwise, add to words to be checked if not repeated
				}
			}
		}
	}
	
	// Adds word to dictionary
	public void addToDictionary () {
		if (wordIndex < wordsToCheck.size()) {
			String currentWord = wordsToCheck.get(wordIndex);		// Set currentWord equal to each word in list
			wordsAdded.add(currentWord);							// Add currentWord to wordsAdded list if added
			wordIndex++;											// Increment word index
		}
	}	
		
	// Do not add word to dictionary
	public void noAdd () {
		if (wordIndex < wordsToCheck.size()) {
			String currentWord = wordsToCheck.get(wordIndex);		// Set currentWord equal to each word in list
			wordsNotAdded.add(currentWord);							// Add currentWord to wordsNotAdded list
			wordIndex++;											// Increment word index
		}
	}
	
	// Get current fielded word
	public String getWord() {
		if (wordIndex < wordsToCheck.size()) {
			String fieldWord = wordsToCheck.get(wordIndex);
			return fieldWord;
		} else {
			getNewDict();
			return "SPELL CHECK COMPLETE";
		}
	}
	
	public void getNewDict() {
		for (int i = 0; i < wordsAdded.size(); i++) {
			dictionaryWords.add(wordsAdded.get(i));					// Add added words to dictionary list
		}
		
		dictionaryWords = alphabetize(0, dictionaryWords.size(), dictionaryWords);
	}
	
	// Writes word lists to file
	public void write(ArrayList<String> wordsToWrite, String filePath) throws FileNotFoundException {
		
		PrintWriter writer = new PrintWriter(filePath);			// Create new printwriter using filepath
		
		// Iterate through arraylist of words to be written
		for (int i = 0; i < wordsToWrite.size(); i++) {			
			writer.println(wordsToWrite.get(i));				// Print each word on new line
		}
		
		writer.close();											// Close printwriter

	}
	
	public ArrayList<String> alphabetize(int L , int R , ArrayList<String> wordList){
		 int par = partition(L, R, wordList);
	      if (L < par - 1)
	    	  alphabetize(L, par - 1, wordList);
	      if (par < R)
	          alphabetize(par, R, wordList);
		
		return wordList;
		
	}
	public int partition(int L, int R, ArrayList<String> wordList){
		int i=L;
		int j=R;
		String temp="";
		String piv=wordList.get((i+j)/2);
		while(i<=j){
			while(wordList.get(i).compareToIgnoreCase(piv)<0){
				i++;
			}
			while(wordList.get(j).compareToIgnoreCase(piv)>0){
				j--;
			}if(i<=j){
				temp=wordList.get(i);
				wordList.set(i, wordList.get(j));
				wordList.set(j, temp);
				i++;
				j--;
			}
		}
		return i;
	}
	
}